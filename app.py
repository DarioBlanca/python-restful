#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, request, json, jsonify, abort, Response, make_response
import datetime
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
import hashlib
import wtforms_json
from sqlalchemy import not_
from wtforms import Form
from wtforms.fields import FormField, FieldList
from wtforms.validators import Length
from flask_sqlalchemy import SQLAlchemy
from flask_restful import reqparse
from flask import send_file
from json import dumps
from passlib.apps import custom_app_context as pwd_context
import jwt
from flask_cors import CORS
from werkzeug.datastructures import FileStorage
from sqlalchemy import update
import random
import base64
import zlib
from PIL import Image
import cStringIO
import time

database_file = 'mysql://phpmyadmin:hayquesertorero@localhost/revents_app'

url = 'http://localhost:8000' 

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = database_file
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
app.config['SECRET_KEY'] = '\xdd\xe2\xe5\x7f\xb6\xd2j\x9d\xa3\n9\xd7o*\x15g\xdc\xba\xb4\xe0\x103\xe3\xc5'

db = SQLAlchemy(app)
CORS(app)

migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)
wtforms_json.init()

"""
	MODEL
"""


class Model(db.Model):
    """Base SQLAlchemy Model for automatic serialization and
    deserialization of columns and nested relationships.
    Usage::
        >>> class User(Model):
        >>>     id = db.Column(db.Integer(), primary_key=True)
        >>>     email = db.Column(db.String(), index=True)
        >>>     name = db.Column(db.String())
        >>>     password = db.Column(db.String())
        >>>     posts = db.relationship('Post', backref='user', lazy='dynamic')
        >>>     ...
        >>>     default_fields = ['email', 'name']
        >>>     hidden_fields = ['password']
        >>>     readonly_fields = ['email', 'password']
        >>>
        >>> class Post(Model):
        >>>     id = db.Column(db.Integer(), primary_key=True)
        >>>     user_id = db.Column(db.String(), db.ForeignKey('user.id'), nullable=False)
        >>>     title = db.Column(db.String())
        >>>     ...
        >>>     default_fields = ['title']
        >>>     readonly_fields = ['user_id']
        >>>
        >>> model = User(email='john@localhost')
        >>> db.session.add(model)
        >>> db.session.commit()
        >>>
        >>> # update name and create a new post
        >>> validated_input = {'name': 'John', 'posts': [{'title':'My First Post'}]}
        >>> model.set_columns(**validated_input)
        >>> db.session.commit()
        >>>
        >>> print(model.to_dict(show=['password', 'posts']))
        >>> {u'email': u'john@localhost', u'posts': [{u'id': 1, u'title': u'My First Post'}], u'name': u'John', u'id': 1}
    """
    __abstract__ = True

    # Stores changes made to this model's attributes. Can be retrieved
    # with model.changes
    _changes = {}

    def __init__(self, **kwargs):
        kwargs['_force'] = True
        self._set_columns(**kwargs)

    def _set_columns(self, **kwargs):
        force = kwargs.get('_force')

        readonly = []
        if hasattr(self, 'readonly_fields'):
            readonly = self.readonly_fields
        if hasattr(self, 'hidden_fields'):
            readonly += self.hidden_fields

        readonly += [
            'id',
            'created',
            'updated',
            'modified',
            'created_at',
            'updated_at',
            'modified_at',
        ]

        changes = {}

        columns = self.__table__.columns.keys()
        relationships = self.__mapper__.relationships.keys()

        for key in columns:
            allowed = True if force or key not in readonly else False
            exists = True if key in kwargs else False
            if allowed and exists:
                val = getattr(self, key)
                if val != kwargs[key]:
                    changes[key] = {'old': val, 'new': kwargs[key]}
                    setattr(self, key, kwargs[key])

        for rel in relationships:
            allowed = True if force or rel not in readonly else False
            exists = True if rel in kwargs else False
            if allowed and exists:
                is_list = self.__mapper__.relationships[rel].uselist
                if is_list:
                    valid_ids = []
                    query = getattr(self, rel)
                    cls = self.__mapper__.relationships[rel].argument()
                    for item in kwargs[rel]:
                        if 'id' in item and query.filter_by(id=item['id']).limit(1).count() == 1:
                            obj = cls.query.filter_by(id=item['id']).first()
                            col_changes = obj.set_columns(**item)
                            if col_changes:
                                col_changes['id'] = str(item['id'])
                                if rel in changes:
                                    changes[rel].append(col_changes)
                                else:
                                    changes.update({rel: [col_changes]})
                            valid_ids.append(str(item['id']))
                        else:
                            col = cls()
                            col_changes = col.set_columns(**item)
                            query.append(col)
                            db.session.flush()
                            if col_changes:
                                col_changes['id'] = str(col.id)
                                if rel in changes:
                                    changes[rel].append(col_changes)
                                else:
                                    changes.update({rel: [col_changes]})
                            valid_ids.append(str(col.id))

                    # delete related rows that were not in kwargs[rel]
                    for item in query.filter(not_(cls.id.in_(valid_ids))).all():
                        col_changes = {
                            'id': str(item.id),
                            'deleted': True,
                        }
                        if rel in changes:
                            changes[rel].append(col_changes)
                        else:
                            changes.update({rel: [col_changes]})
                        db.session.delete(item)

                else:
                    val = getattr(self, rel)
                    if self.__mapper__.relationships[rel].query_class is not None:
                        if val is not None:
                            col_changes = val.set_columns(**kwargs[rel])
                            if col_changes:
                                changes.update({rel: col_changes})
                    else:
                        if val != kwargs[rel]:
                            setattr(self, rel, kwargs[rel])
                            changes[rel] = {'old': val, 'new': kwargs[rel]}

        return changes

    def set_columns(self, **kwargs):
        self._changes = self._set_columns(**kwargs)
        if 'modified' in self.__table__.columns:
            self.modified = datetime.datetime.utcnow()
        if 'updated' in self.__table__.columns:
            self.updated = datetime.datetime.utcnow()
        if 'modified_at' in self.__table__.columns:
            self.modified_at = datetime.datetime.utcnow()
        if 'updated_at' in self.__table__.columns:
            self.updated_at = datetime.datetime.utcnow()
        return self._changes

    @property
    def changes(self):
        return self._changes

    def reset_changes(self):
        self._changes = {}

    def to_dict(self, show=None, hide=None, path=None, show_all=None):
        """ Return a dictionary representation of this model.
        """

        if not show:
            show = []
        if not hide:
            hide = []
        hidden = []
        if hasattr(self, 'hidden_fields'):
            hidden = self.hidden_fields
        default = []
        if hasattr(self, 'default_fields'):
            default = self.default_fields

        ret_data = {}

        if not path:
            path = self.__tablename__.lower()

            def prepend_path(item):
                item = item.lower()
                if item.split('.', 1)[0] == path:
                    return item
                if len(item) == 0:
                    return item
                if item[0] != '.':
                    item = '.%s' % item
                item = '%s%s' % (path, item)
                return item
            show[:] = [prepend_path(x) for x in show]
            hide[:] = [prepend_path(x) for x in hide]

        columns = self.__table__.columns.keys()
        relationships = self.__mapper__.relationships.keys()
        properties = dir(self)

        for key in columns:
            check = '%s.%s' % (path, key)
            if check in hide or key in hidden:
                continue
            if show_all or key is 'id' or check in show or key in default:
                ret_data[key] = getattr(self, key)

        for key in relationships:
            check = '%s.%s' % (path, key)
            if check in hide or key in hidden:
                continue
            if show_all or check in show or key in default:
                hide.append(check)
                is_list = self.__mapper__.relationships[key].uselist
                if is_list:
                    ret_data[key] = []
                    for item in getattr(self, key):
                        ret_data[key].append(item.to_dict(
                            show=show,
                            hide=hide,
                            path=('%s.%s' % (path, key.lower())),
                            show_all=show_all,
                        ))
                else:
                    if self.__mapper__.relationships[key].query_class is not None:
                        ret_data[key] = getattr(self, key).to_dict(
                            show=show,
                            hide=hide,
                            path=('%s.%s' % (path, key.lower())),
                            show_all=show_all,
                        )
                    else:
                        ret_data[key] = getattr(self, key)

        for key in list(set(properties) - set(columns) - set(relationships)):
            if key.startswith('_'):
                continue
            check = '%s.%s' % (path, key)
            if check in hide or key in hidden:
                continue
            if show_all or check in show or key in default:
                val = getattr(self, key)
                try:
                    ret_data[key] = json.loads(json.dumps(val))
                except:
                    pass

        return ret_data


# Tabla intermedia
usuarios_actividades = db.Table('asistencias',
                                db.Column('usuario_id', db.Integer, db.ForeignKey(
                                    'usuario.id'), primary_key=True),
                                db.Column('actividad_id', db.Integer, db.ForeignKey(
                                    'actividad.id'), primary_key=True)
                                )

usuarios_servicios = db.Table('servicios',
                              db.Column('usuario_id', db.Integer, db.ForeignKey(
                                  'usuario.id'), primary_key=True),
                              db.Column('servicio_id', db.Integer, db.ForeignKey(
                                  'servicio.id'), primary_key=True)
                              )

# Tabla


class Usuario(Model):
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(50))
    password = db.Column(db.Text, nullable=False)
    apellido = db.Column(db.String(50))
    email = db.Column(db.String(50), unique=True, nullable=False)
    avatar = db.Column(db.String(200))
    actividades_creadas = db.relationship(
        'Actividad', backref='creador', lazy='dynamic')
    asistira = db.relationship(
        'Actividad', secondary=usuarios_actividades, lazy='dynamic', backref='asistentes')
    servicios_creados = db.relationship(
        'Servicio', backref='creador', lazy='dynamic')
    servicios = db.relationship(
        'Servicio', secondary=usuarios_servicios, lazy='dynamic', backref='clientes')

    default_fields = ['nombre', 'apellido', 'email']
    hidden_fields = ['id', 'password']

    def hash_password(self):
        self.password = hashlib.sha224(self.password).hexdigest()

    def verify_password(self, password):
        return hashlib.sha224(password).hexdigest() == self.password

    def encode_auth_token(self, user_id):
        """
        Generates the Auth Token
        :return: string
        """
        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(days=1, seconds=0),
                'iat': datetime.datetime.utcnow(),
                'sub': user_id
            }
            return jwt.encode(
                payload,
                app.config.get('SECRET_KEY'),
                algorithm='HS256'
            )
        except Exception as e:
            return e

    @staticmethod
    def decode_auth_token(auth_token):
        """
        Decodes the auth token
        :param auth_token:
        :return: integer|string
        """
        try:
            payload = jwt.decode(auth_token, app.config.get('SECRET_KEY'))
            return payload['sub']
        except jwt.ExpiredSignatureError:
            return 'Signature expired. Please log in again.'
        except jwt.InvalidTokenError:
            return 'Invalid token. Please log in again.'

# Tabla


class Actividad(Model):
    id = db.Column(db.Integer, primary_key=True)
    titulo = db.Column(db.String(100), nullable=False)
    descripcion = db.Column(db.Text, default="Sin descripción")
    fecha_creacion = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    fecha_ejecucion = db.Column(db.DateTime)
    precio = db.Column(db.Float)
    creador_id = db.Column(db.Integer, db.ForeignKey(
        'usuario.id'), nullable=False)
    imagen = db.Column(db.String(50))
    comentarios = db.relationship('Comentario', lazy='dynamic', backref='actividad')
    default_fields = ['titulo', ' descripcion', 'fecha_creacion',
                      'fecha_ejecucion', 'precio', 'creador_id']
    hidden_fields = ['id']

    def to_dict(self):
        response = {
            'id': self.id,
            'titulo': self.titulo,
            'descripcion': self.descripcion,
            'fecha_creacion': self.fecha_creacion,
            'fecha_ejecucion': self.fecha_ejecucion,
            'precio': self.precio,
            'imagen': self.imagen
        }
        return response

    def __eq__(self, other):
        if isinstance(other, Actividad):
            return self.id == other.id
        return False

    def __ne__(self, other):
        if isinstance(other, Actividad):
            return self.id != other.id
        return False


class Comentario(Model):
    id = db.Column(db.Integer, primary_key=True)
    texto = db.Column(db.Text, nullable=False)
    creador_id = db.Column(db.Integer, db.ForeignKey('usuario.id'), nullable=False)
    actividad_id = db.Column(db.Integer, db.ForeignKey('actividad.id'), nullable=False)
    fecha = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    def to_dict(self):
        response = {
            'id': self.id,
            'texto': self.texto,
        }
        return response


# Tabla
class Servicio(Model):
    id = db.Column(db.Integer, primary_key=True)
    titulo = db.Column(db.String(100), nullable=False)
    descripcion = db.Column(db.Text, default="Sin descripción")
    fecha_creacion = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    a_partir_de = db.Column(db.DateTime)
    horarios_disponibles = db.Column(db.DateTime)
    precio = db.Column(db.Float)
    creador_id = db.Column(db.Integer, db.ForeignKey(
        'usuario.id'), nullable=False)
    imagen = db.Column(db.String(50))

    default_fields = ['titulo', 'descripcion', 'fecha_creacion',
                      'a_partir_de', 'horarios_disponibles', 'precio', 'creador_id']
    hidden_fields = ['id']


class Mensaje(Model):
    id = db.Column(db.Integer, primary_key=True)
    texto = db.Column(db.Text, nullable=False)
    creador_id = db.Column(db.Integer, db.ForeignKey('usuario.id'), nullable=False)
    receptor_id = db.Column(db.Integer, db.ForeignKey('usuario.id'), nullable=False)
    creador = db.relationship('Usuario', foreign_keys=[creador_id])
    receptor = db.relationship('Usuario', foreign_keys=[receptor_id])
    fecha = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    def to_dict(self):
        response = {
            'id': self.id,
            'texto': self.texto,
        }
        return response

"""
	APIS

"""


@app.route('/usuarios/<int:usuario_id>', methods=['GET'])
def getUsuario(usuario_id):
    usuario = Usuario.query.filter_by(id=usuario_id).first()
    if not usuario:
        return abort(404)

    return jsonify(data=usuario.to_dict())


@app.route('/usuarios/<int:usuario_id>/avatar', methods=['GET'])
def getUsuarioAvatar(usuario_id):
    usuario = Usuario.query.filter_by(id=usuario_id).first()
    if not usuario or usuario.avatar is None:
        return abort(404)
    return send_file("media/" + usuario.avatar, mimetype="image")


@app.route('/usuarios', methods=['GET'])
def allUsuarios():
    usuarios = Usuario.query.all()
    respuesta = {}
    for usuario in usuarios:
        respuesta[usuario.id] = usuario.to_dict()
    return jsonify(respuesta)


@app.route('/actividades/creadas')
def getUsuarioActividadesCreadas():
    token_valido, respuesta = validate_token(request)
    if token_valido:
        actividades = Usuario.query.filter_by(
            id=respuesta).first().actividades_creadas.all()
        respuesta_final = []
        for actividad in actividades:
            respuesta_final.append(actividad.to_dict())
        return jsonify({ 'actividades': respuesta_final })
    else:
        responseObject = {'status': 'failed', 'message': respuesta}
        return make_response(jsonify(responseObject)), 400

@app.route('/actividades/otras')
def getActividadesNoPropias():

    def usuarioAsistira(usuario_id, asistentes):
        for asistente in asistentes:
            print usuario_id
            print asistente.id
            if asistente.id == usuario_id:
                return True
        return False

    token_valido, usuario_id = validate_token(request)
    if token_valido:
        respuesta = []
        actividades = Actividad.query.filter(Actividad.creador_id != usuario_id).all()
        for actividad in actividades:
            esa_actividad = actividad.to_dict()
            esa_actividad['asistira'] = ( "si" if usuarioAsistira(usuario_id, actividad.asistentes) else "no" )
            respuesta.append(esa_actividad)
        return jsonify({ 'actividades' : respuesta })
    else:
        responseObject = {'status': 'failed', 'message': usuario_id}
        return make_response(jsonify(responseObject)), 400
    return "400"

@app.route('/actividades/asistire')
def getActividadesAsistira():
    token_valido, respuesta = validate_token(request)
    if token_valido:
        actividades = Usuario.query.filter_by(id = respuesta).first().asistira
        respuesta = []
        for actividad in actividades:
            respuesta.append(actividad.to_dict())
        return jsonify({ 'actividades' : respuesta})
    else:
        responseObject = {'status': 'failed', 'message': respuesta}
        return make_response(jsonify(responseObject)), 400
    return "400"


baja_asistira_parser = reqparse.RequestParser()
baja_asistira_parser.add_argument('actividad_id', required=True, help="Es necesario un id de activida")
@app.route('/cambioAsistira', methods=['POST'])
def cambioActividadAsistira():
    token_valido, respuesta = validate_token(request)
    if token_valido:
        args = baja_asistira_parser.parse_args()
        usuario = Usuario.query.filter_by(id = respuesta).first()
        actividad = Actividad.query.filter_by(id = args.actividad_id).first()
        if usuario in actividad.asistentes:
            usuario.asistira.remove(actividad)
        else:
            usuario.asistira.append(actividad)
        db.session.commit()
        return "200"
    else:
        responseObject = {'status': 'failed', 'message': respuesta}
        return make_response(jsonify(responseObject)), 400


@app.route('/actividades/<int:actividad_id>/imagen')
def getActivityImagen(actividad_id):
    actividad = Actividad.query.filter_by(id=actividad_id).first()
    if not actividad or actividad.imagen is None:
        return abort(404)
    return send_file(actividad.imagen, mimetype="image")


# USUARIO
@app.route('/profile')
def getPerfil():
    token_valido, respuesta = validate_token(request)
    if token_valido:
        usuario = Usuario.query.filter_by(id = respuesta).first()
        return make_response(jsonify(usuario.to_dict())), 200
    else:
        return make_response(jsonify({ 'status': 'failed', 'message': 'Token no valido' })), 401

usuario_parser = reqparse.RequestParser()
usuario_parser.add_argument('nombre')
usuario_parser.add_argument('apellido')
usuario_parser.add_argument('email', required=True,help='Es necesario un email')
usuario_parser.add_argument('avatar', type=FileStorage, location='files')
usuario_parser.add_argument('password', required=True, help='Es necesaria una contraseña')
@app.route('/nuevoUsuario', methods=['POST'])
def altaUsuario():
    args = usuario_parser.parse_args()
    if args.avatar is not None:
        ruta = str(datetime.now()) + "." + args.avatar.filename.split('.')[1]
        args.avatar.save("media/" + ruta)
        args.avatar = ruta

    usuario = Usuario(**args)
    usuario.hash_password()
    db.session.add(usuario)
    try:
        db.session.commit()
        token = usuario.encode_auth_token(usuario.id)
        responseObject = {'token': token.decode()}
        return make_response(jsonify(responseObject)), 200
    except Exception as e:
        print e
        return abort(Response(dumps({'reason': str(e)}), 500))
    return "1000"


login_parser = reqparse.RequestParser()
login_parser.add_argument('email', required=True, help="Es necesario un email")
login_parser.add_argument('password', required=True, help="Es necesaria una contraseña")
@app.route('/login', methods=['POST', 'GET'])
def login():
    for elemento in request.args:
        print elemento
    args = login_parser.parse_args()
    try:
        user = Usuario.query.filter_by(email=args.email).first()
        if user and user.verify_password(args.password):
            token = user.encode_auth_token(user.id)
            if token:
                responseObject = {'status': 'success', 'message':'Successfully logged in.',
                                     'token': token.decode(), 'name': user.nombre}
                return make_response(jsonify(responseObject)), 200
        else:
			responseObject = {'status': 'failed', 'message': 'Failed to login, usuario o contraseña incorrectos'}
			return make_response(jsonify(responseObject)), 401
    except Exception as e:
        print e
        responseObject = {'status': 'failed'}
        return make_response(jsonify(responseObject)), 500
    return "Vaya uno a saber"


@app.route('/activities')
def getAllActivies():
    token_valido, respuesta = validate_token(request)
    if token_valido:

        activities = Actividad.query.all()
        result = []
        for activity in activities:
            diccionario = activity.to_dict();
            if (diccionario['imagen']):
                diccionario['imagen'] = url + '/actividades/' + str(diccionario['id']) + '/imagen'
            
            usuario = Usuario.query.filter_by(id=respuesta).first()
            diccionario['asistira'] = True if usuario in activity.asistentes else False
            result.append(diccionario)

        return jsonify({'activities': result})
    else:
        responseObject = {'status': 'Invalid token_valido'}
        return make_response(jsonify(responseObject)), 401
    return "999"


activity_parser = reqparse.RequestParser()
activity_parser.add_argument('titulo', required=True, help="Es necesario un titulo")
activity_parser.add_argument('descripcion')
activity_parser.add_argument('fecha_ejecucion')
activity_parser.add_argument('precio', type=int)
activity_parser.add_argument('imagen', type=FileStorage, location='files')
@app.route('/activity', methods=['POST'])
def altaActivity():
    token_valido, respuesta = validate_token(request)
    try:
        if token_valido:
            args = activity_parser.parse_args()
            args['creador_id'] = respuesta
            if ('fecha_ejecucion' in args):
                args['fecha_ejecucion'] = datetime.datetime.strptime(args['fecha_ejecucion'],'%d/%m/%Y %H:%M')
            if (args['imagen']):
                ruta ='./media/' + str(random.randint(0, 9999)) + args['fecha_ejecucion'].strftime('%d-%m-%Y %H:%M') + '.' + args['imagen'].filename.split('.')[1]
                args['imagen'].save(ruta)
                args['imagen'] = ruta
            actividad = Actividad(**args)
            db.session.add(actividad)
            db.session.commit()
            return "200"
        else:
            responseObject = {'status': 'Invalid token_valido'}
        return make_response(jsonify(responseObject)), 401
    except Exception as e:
        print e
        responseObject = {'status': 'failed'}
        return make_response(jsonify(responseObject)), 500

    return "400"

def validate_token(request):
    token = request.headers.get('Authorization')
    if token:
        usuario_id = Usuario.decode_auth_token(token)
        if not isinstance(usuario_id, str):
            return True, usuario_id
        else:
            return False, "Token no valido"

    return False, "Debe haber un token"


editarActivity_parser = reqparse.RequestParser()
editarActivity_parser.add_argument('id', type=int, required=True, help="Es necesario un titulo")
editarActivity_parser.add_argument('titulo', required=True, help="Es necesario un titulo")
editarActivity_parser.add_argument('descripcion')
editarActivity_parser.add_argument('fecha_ejecucion')
editarActivity_parser.add_argument('precio', type=int)
editarActivity_parser.add_argument('imagen', type=FileStorage, location='files')
@app.route('/editActividad/<int:id>', methods=['POST'])
def editActividad(id):
    token_valido, respuesta = validate_token(request)
    try:
        if token_valido:
            args = activity_parser.parse_args()
            if ('fecha_ejecucion' in args):
                args['fecha_ejecucion'] = datetime.datetime.strptime(args['fecha_ejecucion'],'%d/%m/%Y %H:%M')
            actividad = Actividad.query.filter_by(id=id).first()
            actividad = Actividad(**args)
            x = update(Actividad).where(Actividad.id == id).values(**args)
            db.session.execute(x)
            db.session.commit()
            return "200"
        else:
            responseObject = {'status': 'Invalid token_valido'}
        return make_response(jsonify(responseObject)), 401
    except Exception as e:
        print e
        responseObject = {'status': 'failed'}
        return make_response(jsonify(responseObject)), 500

    return "400"

@app.route('/bajaActividad/<int:actividad_id>')
def bajaActividad(actividad_id):
    token_valido, respuesta = validate_token(request)
    try:
        if token_valido:
            actividad = Actividad.query.filter_by(id=actividad_id).first()
            db.session.delete(actividad)
            db.session.commit()
            return "200"
        else:
            responseObject = {'status': 'Invalid token_valido'}
            return make_response(jsonify(responseObject)), 401
    except Exception as e:
        print e
        responseObject = {'status': 'failed'}
        return make_response(jsonify(responseObject)), 500

    return "400"

#COMENTARIOS

nuevo_commentario_parser = reqparse.RequestParser()
nuevo_commentario_parser.add_argument('texto', required=True, help='El comentario no puede ser vacio')
nuevo_commentario_parser.add_argument('actividad_id', required=True, help='Falta especificar la actividad a la que corresponde el comentario')
@app.route('/newComentario', methods=['POST'])
def newComentario():
    token_valido, respuesta = validate_token(request)
    print token_valido, respuesta
    if (token_valido):
        args = nuevo_commentario_parser.parse_args()
        actividad = Actividad.query.filter_by(id = args['actividad_id']).first()
        usuario = Usuario.query.filter_by(id = respuesta).first()
        comentario = Comentario()
        comentario.texto = args['texto']
        comentario.actividad_id = actividad.id
        comentario.creador_id = usuario.id
        actividad.comentarios.append(comentario)
        db.session.commit()
        comentario_final = comentario.to_dict()
        comentario_final['creador'] = usuario.to_dict()
        responseObject = { 'status': 'ok', 'comentario': comentario_final }
        return make_response(jsonify(responseObject)), 200
    else:
        responseObject = {'status': 'Invalid token_valido'}
        return make_response(jsonify(responseObject)), 401

@app.route('/getComentarios/<int:id_actividad>')
def getComentarios(id_actividad):
    token_valido, respuesta = validate_token(request)
    print token_valido, respuesta
    if token_valido:
        actividad = Actividad.query.filter_by(id = id_actividad).first()
        resultado = []
        for comentario in actividad.comentarios:
            elemento = comentario.to_dict()
            creador = Usuario.query.filter_by(id = comentario.creador_id).first()
            elemento['creador'] = creador.to_dict()
            resultado.append(elemento)
        resultado = { 'comentarios': resultado }
        return make_response(jsonify(resultado)), 200
    else:
        responseObject = {'status': 'Invalid token_valido'}
        return make_response(jsonify(responseObject)), 401        

if __name__ == "__main__":
    #manager.run()
    app.run(debug=True, port=8000)
